//
//  ViewController.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 11/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class ViewController: UIViewController{

    @IBOutlet weak var tableView: UITableView!
    
    var earthToneTemplatesPic: [UIImage] = [#imageLiteral(resourceName: "earthyTone1"), #imageLiteral(resourceName: "earthyTone2"), #imageLiteral(resourceName: "earthyTone3"), #imageLiteral(resourceName: "earthyTone4"), #imageLiteral(resourceName: "earthyTone5")]
    
    var roseGoldTemplatesPic: [UIImage] = [#imageLiteral(resourceName: "roseGold1"), #imageLiteral(resourceName: "roseGold2"), #imageLiteral(resourceName: "roseGold3"), #imageLiteral(resourceName: "roseGold4"), #imageLiteral(resourceName: "roseGold5")]
    
    var cleanModernTemplatesPic: [UIImage] = [#imageLiteral(resourceName: "1"), #imageLiteral(resourceName: "cleanModern2"), #imageLiteral(resourceName: "cleanModern3"), #imageLiteral(resourceName: "cleanModern4"), #imageLiteral(resourceName: "assets-1")]
    
    var titleThemes: [String] = ["Clean Modern", "Rose Gold", "Earthy Tones"]
    
    var earthToneTemplates: [Template] = [templateDavia2, templateAbby1, templateNixi2, template4, templateMikey3]
    var roseGoldTemplates: [Template] = [templateAbby4,template3,templateAbby3,templateMikey2,templateMikey1]
    var cleanModernTemplates: [Template] = [template1,templateAbby2,templateDavia3,templateDavia4,templateDavia1]

    
    var currentTheme: Int!
    var templateToDisplay: Template!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupUI()
        
        setUpNavigation()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if (tapGestureRecognizer.view as? UIImageView) != nil{
            print("image tapped")
            performSegue(withIdentifier: "goToProfile", sender: self)
            self.imageView.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.imageView.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.imageView.isHidden = true
    }
    
    private let imageView = UIImageView(image: UIImage(named: "profile-icon-loggedout"))
    
    private struct Const {
    // Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 40
    // Margin from right anchor of safe area to right anchor of Image
        static let ImageRightMargin: CGFloat = 16
    // Margin from bottom anchor of NavBar to bottom anchor of Image for Large NavBar state
        static let ImageBottomMarginForLargeState: CGFloat = 12
    // Margin from bottom anchor of NavBar to bottom anchor of Image for Small NavBar state
        static let ImageBottomMarginForSmallState: CGFloat = 6
    // Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 32
    // Height of NavBar for Small state. Usually it's just 44
        static let NavBarHeightSmallState: CGFloat = 44
    // Height of NavBar for Large state. Usually it's just 96.5 but if you have a custom font for the title, please make sure to edit this value since it changes the height for Large state of NavBar
        static let NavBarHeightLargeState: CGFloat = 96.5
    }
    
    private func setupUI() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "Discover Themes"
    // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        navigationBar.addSubview(imageView)
        imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -Const.ImageRightMargin),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
            ])
        
    }
    
    private func moveAndResizeImage(for height: CGFloat) {
    let coeff: CGFloat = {
    let delta = height - Const.NavBarHeightSmallState
    let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
    return delta / heightDifferenceBetweenStates
        }()
    let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState
    let scale: CGFloat = {
    let sizeAddendumFactor = coeff * (1.0 - factor)
    return min(1.0, sizeAddendumFactor + factor)
        }()
    // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
    let yTranslation: CGFloat = {
    /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
    return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
    let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDetail" {
             let vc = segue.destination as! DetailVC
             vc.templateSender = sender as? IndexPath
        }
        if segue.identifier == "toDesignPage" {
            let vc = segue.destination as! DesignVC
            vc.templateToDisplay = sender as? Template
        }
    }
    
    func setUpNavigation() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
}

// Tableview 
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoverTableViewCell", for: indexPath) as! DiscoverTableViewCell
        
        if indexPath.row == 0 {
            cell.configure(with: cleanModernTemplatesPic, template: cleanModernTemplates)
        }
        if indexPath.row == 1 {
            cell.configure(with: roseGoldTemplatesPic, template: roseGoldTemplates)
        }
        if indexPath.row == 2 {
            cell.configure(with: earthToneTemplatesPic, template: earthToneTemplates)
        }
        
        cell.labelThemeTitle.text = self.titleThemes[indexPath.row]
        
        cell.didSelectItemAction = { [weak self] templateToDisplay in
            self?.performSegue(withIdentifier: "toDesignPage", sender: templateToDisplay)
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 235
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToDetail", sender: indexPath)
    }
}
    
