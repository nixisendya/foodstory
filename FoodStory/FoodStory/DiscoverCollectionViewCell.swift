//
//  DiscoverCollectionViewCell.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 25/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class DiscoverCollectionViewCell: UICollectionViewCell {

    @IBOutlet var myImageView: UIImageView!
    
    static let identifier = "DiscoverCollectionViewCell"
    
    static func nib() -> UINib {
        return UINib(nibName: "DiscoverCollectionViewCell",bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with models: UIImage){
        self.myImageView.image = models
    }

}
