//
//  TemplateCollectionViewCell.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 18/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class TemplateCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var templateImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
