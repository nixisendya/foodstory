//
//  DetailVC.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 18/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var templateCollectionView: UICollectionView!
    @IBOutlet weak var templateTitleLabel: UILabel!
    
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    let cellIdentifier = "TemplateCollectionViewCell"
    var templateSender: IndexPath?
    
    var earthToneTemplatesPic: [UIImage] = [#imageLiteral(resourceName: "earthyTone1"), #imageLiteral(resourceName: "earthyTone2"), #imageLiteral(resourceName: "earthyTone3"), #imageLiteral(resourceName: "earthyTone4"), #imageLiteral(resourceName: "earthyTone5")]
    var earthToneTemplates: [Template] = [templateDavia2, templateAbby1, templateNixi2, template4, templateMikey3]
    
    var roseGoldTemplatesPic: [UIImage] = [#imageLiteral(resourceName: "roseGold1"), #imageLiteral(resourceName: "roseGold2"), #imageLiteral(resourceName: "roseGold3"), #imageLiteral(resourceName: "roseGold4"), #imageLiteral(resourceName: "roseGold5")]
    var roseGoldTemplates: [Template] = [templateAbby4,template3,templateAbby3,templateMikey2,templateMikey1]
    
    var cleanModernTemplatesPic: [UIImage] = [#imageLiteral(resourceName: "1"), #imageLiteral(resourceName: "cleanModern2"), #imageLiteral(resourceName: "cleanModern3"), #imageLiteral(resourceName: "cleanModern4"), #imageLiteral(resourceName: "assets-1")]
    var cleanModernTemplates: [Template] = [template1,templateAbby2,templateDavia3,templateDavia4,templateDavia1]

    var templates:[UIImage] = []
    var templatesName: [Template] = []
    
    var templateToDisplay: Template!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(templateSender)
        // Set Theme Picked
        setThemePicked()
        
        // Set Up CollectionView
        setUpCollectionView()
        
        // Hide Tab Bar in Detail Paga
        self.tabBarController?.tabBar.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setUpCollectionViewItemSize()
        
    }
    
    func setThemePicked() {
        let theme = templateSender![1]

        switch theme {
        case 0:
            self.navigationItem.title = "Clean Modern"
            templates = cleanModernTemplatesPic
            templatesName = cleanModernTemplates

        case 1:
            self.navigationItem.title = "Rose Gold"
            templates = roseGoldTemplatesPic
            templatesName = roseGoldTemplates

        case 2:
            self.navigationItem.title = "Earthy Tone"
            templates = earthToneTemplatesPic
            templatesName = earthToneTemplates

        default:
            print("Didn't find any theme")
        }
    }
    
    private func setUpCollectionView() {
        templateCollectionView.delegate = self
        templateCollectionView.dataSource = self
        let nib = UINib(nibName: "TemplateCollectionViewCell", bundle: nil)
        templateCollectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        
    }
    
    private func setUpCollectionViewItemSize() {
        if collectionViewFlowLayout == nil {
            let screenWidth = view.bounds.width
            let lineSpacing: CGFloat = 0
            let interItemSpacing: CGFloat = 0
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            let width = screenWidth / 2 - 8
            let height = 1.7 * width
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing

            templateCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDesignPage" {
            let vc = segue.destination as! DesignVC
            vc.templateToDisplay = templateToDisplay
        }
    }
    
}

extension DetailVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return templates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! TemplateCollectionViewCell
        
        cell.templateImageView.image = templates[indexPath.item]
        
        // Configure the cell
        cell.layer.shadowColor = UIColor(red: 0.72, green: 0.77, blue: 0.83, alpha: 0.70).cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 10/2
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let template = templates[indexPath.item]
        templateToDisplay = templatesName[indexPath.item]
        performSegue(withIdentifier: "toDesignPage", sender: template)
        print("didSelectItemAt: \(indexPath)")
    }
    

}
