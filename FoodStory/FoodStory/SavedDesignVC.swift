//
//  SavedDesignViewController.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 18/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit



class SavedDesignVC: UIViewController{
    
    @IBOutlet weak var saveDesignCollectionView: UICollectionView!
    
    let cellIdentifier = "savedCellIdentifier"
    let images = ["1","2","3","4","5","6","7","8"]
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    let defaults = UserDefaults.standard
    
    var imagesDirectoryPath:String!
    var images2:[UIImage] = []
    var arr:[String] = []
    var titles:[String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpCollectionView()
        setupUI()
        let nib = UINib(nibName: "SavedCollectionViewCell", bundle: nil)
        saveDesignCollectionView.register(nib, forCellWithReuseIdentifier: cellIdentifier)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewLoadSetup()

    }
    
    func viewLoadSetup(){
        print("NGELOAD LAGIIIIIIII")
        if (defaults.object(forKey: "directoryImage") != nil) {
            getDesignsFromDirectory()
            saveDesignCollectionView.reloadData()
        } else {
            print("saved design empty")
        }
    }
    
    func getDesignsFromDirectory(){
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        // Get the Document directory path
        let documentDirectoryPath:String = paths[0]
        // Create a new path for the new images folder
        imagesDirectoryPath = documentDirectoryPath+("/SavedDesigns")
        titles = defaults.object(forKey: "directoryImage") as! [String]
        images2.removeAll()
        for image in titles{
            let imagePath = imagesDirectoryPath+("/\(image)")
            print("imagePath: \(imagePath)")
            let data = FileManager.default.contents(atPath: imagePath)
            print("data: \(data)")
            if let image = UIImage(data: data!) {
                images2.append(image)
            }
        }
    }


    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if (tapGestureRecognizer.view as? UIImageView) != nil{
            print("image tapped")
            performSegue(withIdentifier: "goToProfile", sender: self)
            self.imageView.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.imageView.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.imageView.isHidden = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let height = navigationController?.navigationBar.frame.height else { return }
        moveAndResizeImage(for: height)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setUpCollectionViewItemSize()
        
    }
    
    private func setUpCollectionView() {
        saveDesignCollectionView.delegate = self
        saveDesignCollectionView.dataSource = self
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewSavedDesign" {
            let vc = segue.destination as! ViewSavedDesignVC
            vc.imageToDisplay = sender as? UIImage
        }
    }

    private let imageView = UIImageView(image: UIImage(named: "lazysloth"))
    
    private struct Const {
    // Image height/width for Large NavBar state
        static let ImageSizeForLargeState: CGFloat = 40
    // Margin from right anchor of safe area to right anchor of Image
        static let ImageRightMargin: CGFloat = 16
    // Margin from bottom anchor of NavBar to bottom anchor of Image for Large NavBar state
        static let ImageBottomMarginForLargeState: CGFloat = 12
    // Margin from bottom anchor of NavBar to bottom anchor of Image for Small NavBar state
        static let ImageBottomMarginForSmallState: CGFloat = 6
    // Image height/width for Small NavBar state
        static let ImageSizeForSmallState: CGFloat = 32
    // Height of NavBar for Small state. Usually it's just 44
        static let NavBarHeightSmallState: CGFloat = 44
    // Height of NavBar for Large state. Usually it's just 96.5 but if you have a custom font for the title, please make sure to edit this value since it changes the height for Large state of NavBar
        static let NavBarHeightLargeState: CGFloat = 96.5
    }
    
    private func setupUI() {
        navigationController?.navigationBar.prefersLargeTitles = true
        title = "My Designs"
    // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        navigationBar.addSubview(imageView)
        imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -Const.ImageRightMargin),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
            ])
        
    }
    
    private func moveAndResizeImage(for height: CGFloat) {
    let coeff: CGFloat = {
    let delta = height - Const.NavBarHeightSmallState
    let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
    return delta / heightDifferenceBetweenStates
        }()
    let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState
    let scale: CGFloat = {
    let sizeAddendumFactor = coeff * (1.0 - factor)
    return min(1.0, sizeAddendumFactor + factor)
        }()
    // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
    let yTranslation: CGFloat = {
    /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
    return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
    let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
    
    
    private func setUpCollectionViewItemSize() {
        if collectionViewFlowLayout == nil {
            let screenWidth = view.bounds.width
            let lineSpacing: CGFloat = 0
            let interItemSpacing: CGFloat = 0
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            let width = screenWidth / 2 - 8
            let height = 1.7 * width
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            saveDesignCollectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
}
    
extension SavedDesignVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "savedCellIdentifier", for: indexPath) as! SavedCollectionViewCell
        
        cell.savedImageName.image = nil
        
        cell.savedImageName.image = images2[indexPath.item]

        cell.layer.shadowColor = UIColor(red: 0.72, green: 0.77, blue: 0.83, alpha: 0.70).cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 10/2
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let save = images2[indexPath.item]
        performSegue(withIdentifier: "viewSavedDesign", sender: save)
    }
    
}


    

    

