//
//  ViewSavedDesignVC.swift
//  FoodStory
//
//  Created by Davia Belinda Hidayat on 20/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class ViewSavedDesignVC: UIViewController {

    @IBOutlet weak var imageSavedDesign: UIImageView!
    
    var imageToDisplay: UIImage!
    
    @IBOutlet weak var btnSave: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigation()
        
        imageSavedDesign.image = imageToDisplay
        setUpMenuButton()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    

    
    func setUpNavigation() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.navigationBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    
    func setUpMenuButton(){
        let saveBtn = UIButton(type: .custom)
        //saveBtn.tintColor = UIColor.init(named: "black")
        saveBtn.frame = CGRect(x: 0.0, y: 0.0, width: 25, height: 25)
        saveBtn.setImage(UIImage(named: "save"),for: .normal)
        saveBtn.addTarget(self, action: #selector(shareToInstagramStoriesItem), for: UIControl.Event.touchUpInside)

        let saveBarItem = UIBarButtonItem(customView: saveBtn)
        let currWidth = saveBarItem.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = saveBarItem.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        navigationItem.rightBarButtonItem = saveBarItem
    }
    
    @objc func shareToInstagramStoriesItem() {
        //convert main view / UIView to UIImage
        let image = imageToDisplay
        
        // NOTE: you need a different custom URL scheme for Stories, instagram-stories, add it to your Info.plist!
        guard let instagramUrl = URL(string: "instagram-stories://share") else {
            return
        }

        if UIApplication.shared.canOpenURL(instagramUrl) {
            let pasterboardItems = [["com.instagram.sharedSticker.backgroundImage": image as Any]]
            UIPasteboard.general.setItems(pasterboardItems)
            UIApplication.shared.open(instagramUrl)
        } else {
            // Instagram app is not installed or can't be opened, pop up an alert
        }
    }
}
