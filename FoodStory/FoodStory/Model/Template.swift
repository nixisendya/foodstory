//
//  Template.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 21/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import Foundation
import UIKit

struct Template {
    var theme: String
    var backgroundColor: UIColor
    var arrayOfPhotos: [Photo]
    var arrayOfText: [Text]
    var arrayOfSticker: [Sticker]
    var bgColorSuggestions: [UIColor]
    var isStickerFirst: Bool = false
}
