//
//  Text.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 21/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

struct Text {
    var defaultText: String
    var fontColor: UIColor
    var font: UIFont
    var x: CGFloat
    var y: CGFloat
    var angle: Double?
}
