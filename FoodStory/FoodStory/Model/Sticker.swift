//
//  Sticker.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 21/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import Foundation
import CoreGraphics

struct Sticker {
    var name: String
    var width: CGFloat
    var height: CGFloat
    var x: CGFloat
    var y: CGFloat
    var isDeletable: Bool
}
