//
//  DataTemplate.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 22/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import Foundation
import UIKit

var bgColorSuggestRoseGold = [UIColor(red: 0.14, green: 0.14, blue: 0.15, alpha: 1.00),UIColor(red: 0.64, green: 0.42, blue: 0.33, alpha: 1.00), UIColor(red: 0.69, green: 0.49, blue: 0.46, alpha: 1.00), UIColor(red: 0.86, green: 0.69, blue: 0.65, alpha: 1.00), UIColor(red: 0.85, green: 0.76, blue: 0.66, alpha: 1.00)]
var bgColorSuggestCleanModern = [UIColor(red: 0.03, green: 0.11, blue: 0.21, alpha: 1.00), UIColor(red: 0.36, green: 0.52, blue: 0.72, alpha: 1.00), UIColor(red: 0.76, green: 0.83, blue: 0.91, alpha: 1.00), UIColor(red: 0.86, green: 0.83, blue: 0.81, alpha: 1.00), UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1.00)]
var bgColorSuggestEarthyTone = [UIColor(red: 0.54, green: 0.63, blue: 0.45, alpha: 1.00), UIColor(red: 0.62, green: 0.66, blue: 0.58, alpha: 1.00), UIColor(red: 0.80, green: 0.72, blue: 0.52, alpha: 1.00), UIColor(red: 0.85, green: 0.79, blue: 0.69, alpha: 1.00), UIColor(red: 0.98, green: 0.96, blue: 0.92, alpha: 1.00)]


//Template Davia - 1
var photoTemplateDavia1 = Photo(width: 263, height: 395, x: 0, y: 93, isRawPhoto: true)
var textTemplateDavia1 = Text(defaultText: "thank you!", fontColor: UIColor(red: 0.80, green: 0.64, blue: 0.44, alpha: 1.00), font: UIFont(name: "Davia-Regular", size: 40.0)!, x: 153, y: 517)
var stickerDavia1 = Sticker(name: "stickerDavia1", width: 168, height: 242, x: 166, y: 0, isDeletable: false)
var stickerDavia2 = Sticker(name: "stickerDavia2", width: 263, height: 395, x: 12, y: 105, isDeletable: false)
var templateDavia1 = Template(theme: "Clean Modern", backgroundColor: UIColor(red: 0.11, green: 0.09, blue: 0.11, alpha: 1.00), arrayOfPhotos: [photoTemplateDavia1], arrayOfText: [textTemplateDavia1], arrayOfSticker: [stickerDavia1, stickerDavia2], bgColorSuggestions: bgColorSuggestCleanModern)

//Template Davia - 2
var photoTemplateDavia2 = Photo(width: 218, height: 346, x: 57, y: 123, isRawPhoto: true)
var textTemplateDavia2 = Text(defaultText: "thank you for order!", fontColor: UIColor(red: 0.80, green: 0.64, blue: 0.44, alpha: 1.00), font: UIFont(name: "sweetpurple", size: 25.0)!, x: 90, y: 491)
var stickerDavia3 = Sticker(name: "stickerDavia16", width: 333, height: 321, x: 0, y: 271, isDeletable: false)
var templateDavia2 = Template(theme: "Earth Tone", backgroundColor: UIColor(red: 0.94, green: 0.83, blue: 0.65, alpha: 1.00), arrayOfPhotos: [photoTemplateDavia2], arrayOfText: [textTemplateDavia2,], arrayOfSticker: [stickerDavia3], bgColorSuggestions: bgColorSuggestEarthyTone)

//Template Davia - 3
var photoTemplateDavia3 = Photo(width: 189, height: 258, x: 52, y: 158, isRawPhoto: true)
var textTemplateDavia4 = Text(defaultText: "fresh everyday", fontColor: UIColor(red: 0, green: 0, blue: 0, alpha: 1.00), font: UIFont(name: "OfficialBook-Regular", size: 25.0)!, x: 72, y: 427)
var stickerDavia7 = Sticker(name: "stickerDavia13", width: 210, height: 321, x: 42, y: 146, isDeletable: false)
var stickerDavia8 = Sticker(name: "stickerDavia14", width: 255, height: 50, x: 60, y: 120, isDeletable: false)
var templateDavia3 = Template(theme: "Clean Modern", backgroundColor: UIColor(red: 0.76, green: 0.43, blue: 0.20, alpha: 1.00), arrayOfPhotos: [photoTemplateDavia3], arrayOfText: [textTemplateDavia4], arrayOfSticker: [stickerDavia8, stickerDavia7], bgColorSuggestions: bgColorSuggestCleanModern)

//Template Davia - 4
var photoTemplateDavia5 = Photo(width: 217, height: 326, x: 58, y: 133, isRawPhoto: true)
var textTemplateDavia5 = Text(defaultText: "thank you!", fontColor: UIColor(red: 1, green: 1, blue: 1, alpha: 1.00), font: UIFont(name: "sweetpurple", size: 40.0)!, x: 105, y: 49)
var textTemplateDavia6 = Text(defaultText: "hope you enjoy our food", fontColor: UIColor(red: 1, green: 1, blue: 1, alpha: 1.00), font: UIFont(name: "sweetpurple", size: 25.0)!, x: 80, y: 516)
var stickerDavia9 = Sticker(name: "stickerDavia15", width: 335, height: 417, x: 0, y: 86, isDeletable: false)
var templateDavia4 = Template(theme: "Clean Modern", backgroundColor: UIColor(red: 0.18, green: 0.22, blue: 0.28, alpha: 1.00), arrayOfPhotos: [photoTemplateDavia5], arrayOfText: [textTemplateDavia5, textTemplateDavia6], arrayOfSticker: [stickerDavia9], bgColorSuggestions: bgColorSuggestCleanModern)

// Template Nixi - 1
var photoTemplateNixi1 = Photo(width: 271, height: 398, x: 27, y: 150, isRawPhoto: true)
var textTemplateNixi1 = Text(defaultText: "Freshly Baked", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "PlayfairDisplaySC-BoldItalic", size: 34.0)!, x: 32, y: 32)
var sticker1 = Sticker(name: "TemplateNixi1-1", width: 171, height: 146, x: 162, y: 0, isDeletable: false)
var sticker12 = Sticker(name: "TemplateNixi1-2", width: 155, height: 177, x: 0, y: 415, isDeletable: false)
var template1 = Template(theme: "Clean Modern", backgroundColor: UIColor(red: 1.00, green: 1.00, blue: 0.99, alpha: 1.00), arrayOfPhotos: [photoTemplateNixi1], arrayOfText: [textTemplateNixi1], arrayOfSticker: [sticker1,sticker12], bgColorSuggestions: bgColorSuggestCleanModern)

// Template Nixi 2
var photoTemplateNixi2 = Photo(width: 225, height: 338, x: 53, y: 125, isRawPhoto: true)
var photoTemplateNixi22 = Photo(width: 72, height: 73, x:130, y:25)
var textTemplateNixi2 = Text(defaultText: "thank you for your order!", fontColor: UIColor(red: 0.50, green: 0.45, blue: 0.43, alpha: 1.00), font: UIFont(name: "Bentham", size: 18.0)!, x: 79, y: 474)
var sticker2 = Sticker(name: "TemplateNixi2-1", width: 306, height: 192, x: 8, y: 393, isDeletable: false)
var templateNixi2 = Template(theme: "Earthy Tone", backgroundColor: UIColor(red: 0.96, green: 0.96, blue: 0.94, alpha: 1.00), arrayOfPhotos: [photoTemplateNixi2, photoTemplateNixi22], arrayOfText: [textTemplateNixi2], arrayOfSticker: [sticker2], bgColorSuggestions: bgColorSuggestEarthyTone)


// Template Nixi 3
var photoTemplateNixi3 = Photo(width: 248, height: 373, x: 37, y: 104, isRawPhoto: true)
var photoTemplateNixi32 = Photo(width: 62, height: 62, x: 136, y: 507)
var textTemplateNixi3 = Text(defaultText: "A sweet review from our customer!", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "Allura-Regular", size: 21.0)!, x: 48, y: 63)
var sticker3 = Sticker(name: "TemplateNixi3-1", width: 248, height: 373, x: 52, y: 118, isDeletable: false)
var template3 = Template(theme: "Rose Gold", backgroundColor: UIColor(red: 1.00, green: 0.96, blue: 0.95, alpha: 1.00), arrayOfPhotos: [photoTemplateNixi3,photoTemplateNixi32], arrayOfText: [textTemplateNixi3], arrayOfSticker: [sticker3], bgColorSuggestions: bgColorSuggestRoseGold)

// Template Nixi 4
var photoTemplateNixi4 = Photo(width: 242, height: 430, x: 46, y: 107, isRawPhoto: true)
var textTemplateNixi4 = Text(defaultText: "Hope you enjoyed our food!", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "GlacialIndifference-Bold", size: 21.0)!, x: 37, y: 33)
var textTemplateNixi5 = Text(defaultText: "according to our customer", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "Bentham", size: 18.0)!, x: 70, y: 63)
var sticker4 = Sticker(name: "TemplateNixi4-1", width: 274, height: 463, x: 30, y: 90, isDeletable: false)
var sticker5 = Sticker(name: "TemplateNixi4-2", width: 333, height: 349, x: 0, y: 243, isDeletable: false)
var template4 = Template(theme: "Earthy Tone", backgroundColor: UIColor(red: 0.92, green: 0.90, blue: 0.87, alpha: 1.00), arrayOfPhotos: [photoTemplateNixi4], arrayOfText: [textTemplateNixi4,textTemplateNixi5], arrayOfSticker: [sticker5,sticker4], bgColorSuggestions: bgColorSuggestEarthyTone)

// template mikey

var photoTemplateMikey1 = Photo(width: 233, height: 349, x: 71, y: 125, isRawPhoto: true)
var textTemplateMikey1 = Text(defaultText: " ", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "Times New Roman", size: 19.0)!, x: 155, y: 206)
var stickerMikey1 = Sticker(name: "Rectangle", width: 278, height: 454, x: 48, y: 72, isDeletable: false)
var stickerMikey2 = Sticker(name: "Group 2", width: 35, height: 455, x: 7, y: 72, isDeletable: false)
var templateMikey1 = Template(theme: "Rose Gold", backgroundColor: UIColor(red: 1.00, green: 0.81, blue: 0.73, alpha: 1.00), arrayOfPhotos: [photoTemplateMikey1], arrayOfText: [textTemplateMikey1], arrayOfSticker: [stickerMikey1,stickerMikey2], bgColorSuggestions: bgColorSuggestRoseGold)

var photoTemplateMikey2 = Photo(width: 240, height: 427, x: 46, y: 74, isRawPhoto: true)
var textTemplateMikey2 = Text(defaultText: " ", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "Times New Roman", size: 19.0)!, x: 155, y: 206)
var stickerMikey3 = Sticker(name:"Rectangle2", width: 252, height: 518, x: 39, y: 42, isDeletable: false)
var stickerMikey4 = Sticker(name: "bg 2 ", width: 333, height: 592, x: 0, y: 0, isDeletable: false)
var stickerMikey5 = Sticker(name: "Triangle", width: 98, height: 100, x: 0, y: 0, isDeletable: false)
var stickerMikey6 = Sticker(name: "Triangle 2", width: 98, height: 100, x: 233, y: 0, isDeletable: false)
var stickerMikey7 = Sticker(name: "Triangle 3", width: 98, height: 100, x: 0, y: 491, isDeletable: false)
var stickerMikey8 = Sticker(name: "Triangle 4", width: 98, height: 100, x: 233, y: 491, isDeletable: false)
var templateMikey2 = Template(theme: "Rose Gold", backgroundColor: UIColor(red: 0.92, green: 0.73, blue: 0.73, alpha: 1.00)
, arrayOfPhotos: [photoTemplateMikey2], arrayOfText: [textTemplateMikey2], arrayOfSticker: [stickerMikey3,stickerMikey4,stickerMikey5,stickerMikey6, stickerMikey7, stickerMikey8], bgColorSuggestions: bgColorSuggestRoseGold)

var photoTemplateMikey3 = Photo(width: 220, height: 220, x: 60, y: 180, angle: -44)
var textTemplateMikey31 = Text(defaultText: "GOOD ", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "Times New Roman", size: 19.0)!, x: 100, y: 110, angle: -45)
var textTemplateMikey32 = Text(defaultText: "FOOD ", fontColor: UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00), font: UIFont(name: "Times New Roman", size: 19.0)!, x: 184, y: 435, angle: -45)
var stickerMikey9 = Sticker(name: "R2", width: 333, height: 333, x: 1, y: 125, isDeletable: false)
var stickerMikey10 = Sticker(name: "R3", width: 0, height: 0, x: 333, y: 592, isDeletable: false)
var templateMikey3 = Template(theme: "Earthy Tone", backgroundColor: UIColor(red: 0.94, green: 0.93, blue: 0.53, alpha: 1.00), arrayOfPhotos: [photoTemplateMikey3], arrayOfText: [textTemplateMikey31,textTemplateMikey32], arrayOfSticker: [stickerMikey9,stickerMikey10], bgColorSuggestions: bgColorSuggestEarthyTone)



// Template Abby-1
var photoTemplateAbby1 = Photo(width: 224, height: 349, x: 54, y: 77, isRawPhoto: true)
var textTemplateAbby1 = Text(defaultText: "freshly handmade", fontColor: UIColor(red: 0.32, green: 0.21, blue: 0.11, alpha: 1.00), font: UIFont(name: "CrimsonText-Italic", size: 22.0)!, x: 66, y: 445)
var stickerAbby1a = Sticker(name: "stickerAbby1a", width: 333, height: 592, x: 0, y: 0, isDeletable: false)
var stickerAbby1b = Sticker(name: "stickerAbby1b", width: 333, height: 592, x: 0, y: 0, isDeletable: false)
var templateAbby1 = Template(theme: "Earthy Tone", backgroundColor: UIColor(red: 0.96, green: 0.95, blue: 0.89, alpha: 1.00), arrayOfPhotos: [photoTemplateAbby1], arrayOfText: [textTemplateAbby1], arrayOfSticker: [stickerAbby1a, stickerAbby1b], bgColorSuggestions: bgColorSuggestEarthyTone)

// Template Abby-2
var photoTemplateAbby2a = Photo(width: 333, height: 344, x: 0, y: 0)
var photoTemplateAbby2b = Photo(width: 62, height: 62, x: 135, y: 406)
var textTemplateAbby2a = Text(defaultText: "it’s the best coffee in town I highly recommend it!", fontColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00), font: UIFont(name: "Helvetica", size: 13.0)!, x: 85, y: 309)
var textTemplateAbby2b = Text(defaultText: "— Sarah Baker, Coffee Enthusiast", fontColor: UIColor(red: 0.00, green: 0.00, blue: 0.00, alpha: 1.00), font: UIFont(name: "CrimsonText-Italic", size: 12.0)!, x: 88, y: 487)
var stickerAbby2 = Sticker(name: "stickerAbby2",width: 333, height: 592, x: 0, y: 0, isDeletable: false)
var templateAbby2 = Template(theme: "Earthy Tone", backgroundColor: UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00), arrayOfPhotos: [photoTemplateAbby2a, photoTemplateAbby2b], arrayOfText: [textTemplateAbby2a, textTemplateAbby2b], arrayOfSticker: [stickerAbby2], bgColorSuggestions: bgColorSuggestCleanModern)

// Template Abby-3
var photoTemplateAbby3a = Photo(width: 156, height: 156, x: 55, y: 70, angle: 355)
var photoTemplateAbby3b = Photo(width: 156, height: 156, x: 127, y: 333, angle: 5)
var textTemplateAbby3a = Text(defaultText: "box of cookies", fontColor: UIColor(red: 0.39, green: 0.37, blue: 0.49, alpha: 1.00), font: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!, x: 90, y: 240, angle: 355)
var textTemplateAbby3b = Text(defaultText: "loyal customer", fontColor: UIColor(red: 0.39, green: 0.37, blue: 0.49, alpha: 1.00), font: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!, x: 152, y: 508, angle: 5)
var stickerAbby3 = Sticker(name: "stickerAbby3", width: 333, height: 592, x: 0, y: 0, isDeletable: false)
var templateAbby3 = Template(theme: "Earthy Tone", backgroundColor: UIColor(red: 0.98, green: 0.91, blue: 0.91, alpha: 1.00), arrayOfPhotos: [photoTemplateAbby3a, photoTemplateAbby3b], arrayOfText: [textTemplateAbby3a, textTemplateAbby3b], arrayOfSticker: [stickerAbby3], bgColorSuggestions: bgColorSuggestRoseGold)

// Template Abby-4
var photoTemplateAbby4 = Photo(width: 175, height: 231, x: 79, y: 135, isRawPhoto: true)
var textTemplateAbby4a = Text(defaultText: "enjoy your meal", fontColor: UIColor(red: 0.34, green: 0.27, blue: 0.25, alpha: 1.00), font: UIFont(name: "Cherolina", size: 26.0)!, x: 123, y: 370)
var textTemplateAbby4b = Text(defaultText: "THANK YOU", fontColor: UIColor(red: 0.92, green: 0.72, blue: 0.27, alpha: 1.00), font: UIFont(name: "cooperblackstd", size: 31.0)!, x: 60, y: 480)
var stickerAbby4 = Sticker(name: "stickerAbby4", width: 333, height: 592, x: 0, y: 0, isDeletable: false)
var templateAbby4 = Template(theme: "Rose Gold", backgroundColor: UIColor(red: 0.91, green: 0.76, blue: 0.69, alpha: 1.00), arrayOfPhotos: [photoTemplateAbby4], arrayOfText: [textTemplateAbby4a, textTemplateAbby4b], arrayOfSticker: [stickerAbby4], bgColorSuggestions: bgColorSuggestRoseGold)


