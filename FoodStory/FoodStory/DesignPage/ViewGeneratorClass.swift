//
//  ViewGeneratorClass.swift
//  FoodStory
//
//  Created by Adhitya Yoga Yudanto on 21/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import Foundation
import UIKit

struct MainStruct {
    var arrText: [CustomText] = []
    var arrPhoto: [CustomPhoto] = []
}

struct CustomText {
    var defaultText: String
    var fontSize: Int
    var fontColor: String //r g b and alpa in hex format
    var fontType: String
    var x: Int
    var y: Int
}

struct CustomPhoto {
    var imageView: String
    var width: Int
    var height: Int
    var x: Int
    var y: Int
    var isDeletAble: Bool
}

class ViewGenerator{
    var mainView:UIView
    var imagePicker: ImagePicker!
    var mainUIViewController: DesignVC
    var tempIsRawPhoto: Bool?
    
    init(mainView: UIView, mainUIViewController: DesignVC, templateName: Template){
        self.mainView = mainView
        self.mainUIViewController = mainUIViewController
        
        procList(param: templateName)
    }
    
    func procList(param: Template) -> Void {
        
        //backgorundColor
        mainView.backgroundColor = param.backgroundColor
        
        //check Sticker first?
        if(!param.isStickerFirst){
            //loop stickers
            for item in param.arrayOfSticker{
                generateStickerItem(param: item)
            }
            
            //loop photos
            for item in param.arrayOfPhotos {
                generatePhotoItem(param: item)
            }
            
            //Loop text
            for item in param.arrayOfText{
                generateTextItem(param: item)
            }
        }
        else{
            //loop photos
            for item in param.arrayOfPhotos {
                generatePhotoItem(param: item)
            }
            
            //Loop text
            for item in param.arrayOfText{
                generateTextItem(param: item)
            }
            
            //loop stickers
            for item in param.arrayOfSticker{
                generateStickerItem(param: item)
            }
        }
        
        
        
    }
    
    func generateTextItem(param: Text) -> Bool {
        //convert view to image
        
        var labelText = UITextField()
        
        
        
        
        //let maxSize = CGSize(width: 10, height: 300)
        //let size = labelText.sizeThatFits(maxSize)
        
        //position label
        //var framex = CGRect()
        //framex.origin = CGPoint(x: param.x, y: param.y)
        //framex.origin = CGPoint(50, 50);
        //labelText.frame = CGRect(origin: CGPoint(x: param.x, y: param.y), size: size);
        
        var myFrame = labelText.frame;
        
        myFrame = CGRect(x: param.x,
                                          y: param.y,
        width: 300,
        height: 50)
        
        labelText.frame = myFrame
        
        //Label for title (overlay)
        //labelText.textAlignment = .center
        labelText.text = param.defaultText
        labelText.textColor = param.fontColor
        labelText.font = param.font
        
        //labelText.numberOfLines = 0
        labelText.sizeToFit()
        
        
        
        //labelText.lineBreakMode = .byWordWrapping
        //labelText.numberOfLines = 0
        //labelText.backgroundColor = .blue
        
        labelText.transform = labelText.transform.rotated(by: CGFloat(rad2deg(param.angle ?? 0)))
        
        //add tap gesture
        let tapGesture = MyTapGesture(target: self, action: #selector(self.tapLabel(sender:)))
        //labelText.addGestureRecognizer(tapGesture)
        
        labelText.isUserInteractionEnabled = true
        
        labelText.autocorrectionType = .no
        
        //set parameter
        //tapGesture.itemLabel = labelText
        
        mainView.addSubview(labelText)
        return true
    }
    
    @objc func tapLabel(sender: MyTapGesture) {
        print(sender)
        
        let itemTextView = UITextField(frame: CGRect(x: sender.itemLabel.frame.minX,//param.width/2,
            y: sender.itemLabel.frame.minY,//param.height/2,
            width: sender.itemLabel.frame.width,
        height: sender.itemLabel.frame.height))
        
        itemTextView.textAlignment = .center
        itemTextView.text = sender.itemLabel.text
        
        mainView.addSubview(itemTextView)
        
    }
    
    func generatePhotoItem(param: Photo) -> Bool {
        //convert view to image
        
        let itemImage = UIImage(named: "button-plus")
        
        let itemImageView = UIImageView(frame: CGRect(x: 0,//param.width/2,
            y: 0,//param.height/2,
            width: param.width,
            height: param.height))
        
        let itemUIView = UIView(frame: CGRect(x: param.x,
                        y: param.y,
                        width: param.width,
                        height: param.height))
        
        
        //itemImageView = UIImageView(frame: itemUIView.bounds)
        itemImageView.contentMode = .center
        //itemImageView.clipsToBounds = false
        //itemImageView.center = itemUIView.center
        itemImageView.image = itemImage
        
        //itemImageView.transform = itemImageView.transform.rotated(by: 90)
        itemUIView.transform = itemUIView.transform.rotated(by: CGFloat(rad2deg(param.angle ?? 0)))
        itemUIView.addSubview(itemImageView)
        itemUIView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        
        //Gesture choose photo
        let tapGesture = MyTapGesture(target: self, action: #selector(self.choosePhoto(sender:)))
        itemUIView.addGestureRecognizer(tapGesture)
        tapGesture.itemImage = itemImageView
        tapGesture.isRawaPhoto = param.isRawPhoto
        
        //Gesture modal
        //itemUIView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.showViewChoosePhoto)))
        
        mainView.addSubview(itemUIView)
        
        
        return true
    }
    
    func rad2deg(_ number: Double) -> Double {
        return (.pi / 180) * number
    }

    //Function for choose photo from gallery
    @objc func choosePhoto(sender : MyTapGesture) {
        print(sender.itemImage)
        tempIsRawPhoto = sender.isRawaPhoto
        mainUIViewController.animateInChoosePhoto(paramImage: sender.itemImage!)
        
        //label1.text = sender.titleK
    }
    
    func callChoosePhoto(param: UIImageView) {
        self.imagePicker = ImagePicker(presentationController: mainUIViewController, tempImage: param)
        imagePicker.present(from: mainView, isRawPhoto: tempIsRawPhoto!)
    }
    
    func generateStickerItem(param: Sticker) -> Bool {
        //convert view to image
        
        let itemImage = UIImage(named: param.name)
        let itemImageView = UIImageView(frame: CGRect(x: param.x,
                                                      y: param.y,
                                                      width: param.width,
                                                      height: param.height))
        
        let itemUIView = UIView(frame: CGRect(x: param.x,
                                                    y: param.y,
                                                    width: param.width,
                                                    height: param.height))
        
        itemImageView.image = itemImage
        //itemImageView = UIImageView(frame: itemUIView.bounds)
        //itemImageView.contentMode =  UIView.ContentMode.scaleAspectFill
        //itemImageView.clipsToBounds = true
        //itemImageView.center = itemUIView.center
        
        //itemUIView.addSubview(itemImageView)
        //itemUIView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        mainView.addSubview(itemImageView)
        
        
        return true
    }
    
    //Note : if show an error about permission, you have to add LSApplicationQueriesSchemes into info.plist
    //Ref : https://developers.facebook.com/docs/instagram/sharing-to-stories
    func shareToInstagramStories() {
        //convert main view / UIView to UIImage
        let image = mainView.asImage()
        
        // NOTE: you need a different custom URL scheme for Stories, instagram-stories, add it to your Info.plist!
        guard let instagramUrl = URL(string: "instagram-stories://share") else {
            return
        }

        if UIApplication.shared.canOpenURL(instagramUrl) {
            let pasterboardItems = [["com.instagram.sharedSticker.backgroundImage": image as Any]]
            UIPasteboard.general.setItems(pasterboardItems)
            UIApplication.shared.open(instagramUrl)
        } else {
            // Instagram app is not installed or can't be opened, pop up an alert
        }
    }
    
    //Note : share UIImage to Instagram
    //Ref : https://developers.facebook.com/docs/instagram/sharing-to-stories
    func shareToInstagramStoriesItem(paramImage: UIImage) {
        //convert main view / UIView to UIImage
        let image = paramImage
        
        // NOTE: you need a different custom URL scheme for Stories, instagram-stories, add it to your Info.plist!
        guard let instagramUrl = URL(string: "instagram-stories://share") else {
            return
        }

        if UIApplication.shared.canOpenURL(instagramUrl) {
            let pasterboardItems = [["com.instagram.sharedSticker.backgroundImage": image as Any]]
            UIPasteboard.general.setItems(pasterboardItems)
            UIApplication.shared.open(instagramUrl)
        } else {
            // Instagram app is not installed or can't be opened, pop up an alert
        }
    }
    
    //Show modal choose photo
    @objc func showViewChoosePhoto(){
        mainView.addSubview(self.mainUIViewController.viewChoosePhoto)
        //self.mainUIViewController.viewChoosePhoto.center = self.mainUIViewController.view.center
        self.mainUIViewController.viewChoosePhoto.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.mainUIViewController.viewChoosePhoto.alpha = 0
        mainUIViewController.navigationController?.navigationBar.alpha = 1
        
        UIView.animate(withDuration: 0.6){
           
            self.mainUIViewController.viewChoosePhoto.alpha = 1
            self.mainUIViewController.viewChoosePhoto.transform = CGAffineTransform.identity
            self.mainUIViewController.navigationController?.navigationBar.alpha = 0
        }
        
    }
    
}

extension UIViewController
{
    func setupToHideKeyboardOnTapOnView()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard2))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard2()
    {
        view.endEditing(true)
    }
}

class MyTapGesture: UITapGestureRecognizer {
    var isRawaPhoto: Bool!
    var itemImage: UIImageView?
    var itemLabel: UILabel!
}

//Convert UIView to UIImage
extension UIView {
   func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: frame.size)
        return renderer.image { context in
            layer.render(in: context.cgContext)
        }
    }
}

//change hex code to UIColor
extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat

        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])

            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0

                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255

                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }

        return nil
    }
}
