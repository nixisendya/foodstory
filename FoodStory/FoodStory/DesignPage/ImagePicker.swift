//
//  ImagePicker.swift
//  FoodStory
//
//  Created by Adhitya Yoga Yudanto on 25/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import Foundation
import UIKit
import CoreML
import Vision

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?)
}

open class ImagePicker: NSObject {
    
    // Create request to CoreML
    lazy var analyzeRequest: VNCoreMLRequest = {
        do {
            let model = try VNCoreMLModel(for: FoodStoryImageClassifier3().model) // Initiate ML model to our request
            let request = VNCoreMLRequest(model: model) { [weak self] (request, error) in
                self?.processToAnalyse(for: request, error: error) //Ask the machine to process evaluate the object request and give result based on ML Model.
            }
            return request
        } catch {
            fatalError("Failed to Load MLModel: \(error)")
        }
    }()
    
    lazy var analyzeRequestWA: VNCoreMLRequest = {
        do {
            let modelWA = try VNCoreMLModel(for: whatsappheader_1().model) // Initiate ML model to our request
            let requestWA = VNCoreMLRequest(model: modelWA) { [weak self] (request, error) in
                self?.processToAnalyseWA(for: request, error: error) //Ask the machine to process evaluate the object request and give result based on ML Model.
                
            }
            requestWA.imageCropAndScaleOption = .scaleFit
            return requestWA
        } catch {
            fatalError("Failed to Load MLModel: \(error)")
        }
    }()
    
    var pathLayer: CALayer?
    private let pickerController: UIImagePickerController
    private weak var presentationController: UIViewController?
    //private weak var delegate: ImagePickerDelegate?
    private weak var tempImage: UIImageView?
    private var tempProcessedImage: UIImage?
    private var isRawPhoto: Bool = false

    public init(presentationController: UIViewController, tempImage: UIImageView) {
        self.pickerController = UIImagePickerController()

        super.init()

        self.presentationController = presentationController
        //self.delegate = delegate

        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.image"]
        self.tempImage = tempImage
        self.pickerController.sourceType = .photoLibrary
        
    }

    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }

        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.presentationController?.present(self.pickerController, animated: true)
        }
    }

    public func present(from sourceView: UIView, isRawPhoto: Bool) {

        self.pickerController.allowsEditing = !isRawPhoto
        self.isRawPhoto = isRawPhoto
        //self.pickerController.sourceType = type
        self.presentationController?.present(self.pickerController, animated: true)
        
        /*
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        /*
        if let action = self.action(for: .camera, title: "Take photo") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        */
        if let action = self.action(for: .photoLibrary, title: "Photo library") {
            alertController.addAction(action)
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sourceView
            alertController.popoverPresentationController?.sourceRect = sourceView.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }

        self.presentationController?.present(alertController, animated: true)
        */
    }

    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        
        if image != nil {
            self.tempImage!.image = image
            self.tempImage?.contentMode = .scaleAspectFit
            //self.delegate?.didSelect(image: image)
            self.convertImageToAnalysed(image: image!)
        }
        
        
    }
}

extension ImagePicker: UIImagePickerControllerDelegate {

    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        
        if isRawPhoto {
            guard let image = info[.originalImage] as? UIImage else {
                return self.pickerController(picker, didSelect: nil)
            }
            tempProcessedImage = image
            self.pickerController(picker, didSelect: image)
        }
        else{
            guard let image = info[.editedImage] as? UIImage else {
                return self.pickerController(picker, didSelect: nil)
            }
            tempProcessedImage = image
            self.pickerController(picker, didSelect: image)
        }
        
        
    }
}

extension ImagePicker: UINavigationControllerDelegate {

}

// MARK: Machine learning process goes here
extension ImagePicker {
    
    // This part of Image processing from Vision
    func convertImageToAnalysed(image: UIImage) {
        //resultLabel.text = "Analysing..."
        let imageProperty = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
        //Conver from UIImage property to CoreImage property -> UIImage >< CIIMage, CIImage is format used by vision
        
        guard let ciImageTemp = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Create background thread with qos property due to heavy task for image processing, so we try to avoid this process under main /UI Thread to avoid leak or crash due to memory leak.
            
            // Create instance for the request vision handler to process image processing
            let handler = VNImageRequestHandler(ciImage: ciImageTemp, orientation: imageProperty!)
            
            do {
                // Perform request based on the handler and analyze it and update
                try handler.perform([self.analyzeRequest])
            } catch {
                print("Failed to perform.")
                
            }
        }
    }
    
    func convertImageToAnalysedWA(image: UIImage) {
        //resultLabel.text = "Analysing..."
        let imagePropertyWA = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
        //Conver from UIImage property to CoreImage property -> UIImage >< CIIMage, CIImage is format used by vision
        
        guard let ciImageTempWA = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        DispatchQueue.global(qos: .userInitiated).async {
            // Create background thread with qos property due to heavy task for image processing, so we try to avoid this process under main /UI Thread to avoid leak or crash due to memory leak.
            
            // Create instance for the request vision handler to process image processing
            let handlerWA = VNImageRequestHandler(ciImage: ciImageTempWA, orientation: imagePropertyWA!)
            print(image.size.width)
            print(image.size.height)
            
            do {
                // Perform request based on the handler and analyze it and update
                try handlerWA.perform([self.analyzeRequestWA])
            } catch {
                print("Failed to perform.")
                
            }
        }
    }
    
    // THis part of Image Classification from CoreML to
    func processToAnalyse(for request: VNRequest, error: Error?){
        DispatchQueue.main.async {
            guard let results = request.results else {
                print("Can not analyse the object.")
                return
            }
            
            // Create instance for obvervations returned by VNCoreMLRequest that use a model that is image classifier. A classifier produces a set of array of classifications which are labels and confidence score. Yang ada di VNClassificationObservation tuh confidence level gtgt.
            let classifications = results as! [VNClassificationObservation]
            
            if classifications.isEmpty {
                print("Nothing to analyze")
            } else {
                let importantInformation = classifications.prefix(2) //Top 2 information from result: ConfidenceValue, IdentifierValue
                let readableStringResult = importantInformation.map { (classification) in
                    return String(format: "(%.2f), %@", classification.confidence, classification.identifier)
                    // Convert key value from classification result given by CoreML decision, to readable string.
                }
                if (readableStringResult[0].contains("WA Screenshot")) {
                    
                    print("ini WA")
                    self.blurHeader(self.tempProcessedImage!)
                } else {
                   // print(classifications)
                    print("ini bukan WA")
                }
                //print(readableStringResult.joined(separator: " | "))
            }
            
        }
        
    }
    
    
    
    func processToAnalyseWA(for request: VNRequest, error: Error?){
        DispatchQueue.main.async {
            guard let results = request.results else {
                print("Can not analyse the object.")
                return
            }
            
            // Create instance for obvervations returned by VNCoreMLRequest that use a model that is image classifier. A classifier produces a set of array of classifications which are labels and confidence score. Yang ada di VNClassificationObservation tuh confidence level gtgt.
            let classifications = results as! [VNRecognizedObjectObservation]
            
            
            
            if classifications.isEmpty {
                print("Nothing to analyze")
            } else {
                let importantInformation = classifications.prefix(2) //Top 2 information from result: ConfidenceValue, IdentifierValue
                let readableStringResult = importantInformation.map { (classification) in
                    return classification.boundingBox
                }
                self.drawDetectionsOnPreview(detections: classifications)

            
            }
        }
        
    }

    func drawDetectionsOnPreview(detections: [VNRecognizedObjectObservation]) {
            guard let image = self.tempImage?.image else {
                return
            }
            
            let imageSize = image.size
            let scale: CGFloat = 0

            image.draw(at: CGPoint.zero)

            for detection in detections {
                
                print(detection.labels.map({"\($0.identifier) confidence: \($0.confidence)"}).joined(separator: "\n"))
                print("------------")
                
    //            The coordinates are normalized to the dimensions of the processed image, with the origin at the image's lower-left corner.
                let boundingBox = detection.boundingBox
                let rectangle = CGRect(x: boundingBox.minX*tempImage!.frame.width, y: (1-boundingBox.minY-boundingBox.height)*tempImage!.frame.height, width: boundingBox.width*tempImage!.frame.width, height: boundingBox.height*tempImage!.frame.height)
                
                print(boundingBox)
                print(rectangle)
                
                
                let newView = UIView()
                newView.backgroundColor = .black
                newView.frame = rectangle
                self.tempImage!.addSubview(newView)
//                let blurEffect = UIBlurEffect(style: .dark)
//                let blurredEffectView = UIVisualEffectView(effect: blurEffect)
//                blurredEffectView.frame = rectangle
//                //newView.layer
//                self.tempImage!.addSubview(blurredEffectView)

                /*
                //test crop
                var newImage = cropImage(image: image, toRect: rectangle)
                newImage = blurImage(usinglmage: newImage!, blurAmount: 5.0)
                print(newImage)
                
                let newUIImageView = UIImageView(frame: rectangle)
                newUIImageView.image = newImage
                self.tempImage!.addSubview(newUIImageView)
                */
            }
            
        }
    
    func blurImage(usinglmage image: UIImage, blurAmount: CGFloat) -> UIImage?
    {
        guard let ciImage = CIImage( image: image) else
        {
            return nil
        }
        let blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        blurFilter?.setValue(blurAmount, forKey: kCIInputRadiusKey)
        guard let outputImage = blurFilter?.outputImage else
        {
            return nil
        }
        
        return UIImage(ciImage: outputImage)
    }
    
    func cropImage(image: UIImage, toRect: CGRect) -> UIImage? {
        // Cropping is available trhough CGGraphics
        let cgImage :CGImage! = image.cgImage
        let croppedCGImage: CGImage! = cgImage.cropping(to: toRect)

        return UIImage(cgImage: croppedCGImage)
    }
    
    func blurHeader(_ image: UIImage){
        //TODO Implement blur header
        self.convertImageToAnalysedWA(image: image)
//        var imageToBlur = CIImage(image: image)
//        var blurfilter = CIFilter(name: "CIGaussianBlur")
//        blurfilter!.setValue(imageToBlur, forKey: "inputImage")
//        var resultImage = blurfilter!.value(forKey: "outputImage") as! CIImage
//        var blurredImage = UIImage(ciImage: resultImage)
//        self.tempProcessedImage! = blurredImage
   
        print("silahkan kerjakan blur header disini")
    }
}
