//
//  DesignVC.swift
//  FoodStory
//
//  Created by Nixi Sendya Putri on 18/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit
import Foundation

class DesignVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    
    @IBOutlet weak var StickerCollectionView: UICollectionView!
    @IBOutlet weak var BackgroundColorCollectionView: UICollectionView!
    @IBOutlet weak var TextColorCollectionView: UICollectionView!
    
   
    @IBOutlet weak var viewCanvas: UIView!
    @IBOutlet var viewTextToolOulet: UIView!
    @IBOutlet weak var textColorBtn: UIButton!
    @IBOutlet weak var viewDesignToolbarOutlet: UIView!
    @IBOutlet var viewColorTextToolbarOutlet: UIView!
    @IBOutlet var viewBackgroundColorToolbar: UIView!
    @IBOutlet var viewStickerToolbarOutlet: UIView!
    @IBOutlet var viewSaveOutlet: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var imagee: UIImageView!
    
    @IBOutlet weak var btnSaveInstagram: UIButton!
    @IBOutlet weak var btnSaveDecide: UIButton!
    
    @IBOutlet var viewChoosePhoto: UIView!
    @IBOutlet weak var textAlignmentOutlet: UIButton!
    
    
    //var arrayOfSavedDesigns: [Data] = []
    var imagesDirectoryPath:String!
    var images:[UIImage]!
    var titles:[String]!
    var directoryToDesigns:[String]!
    var isOpen = 0
    var b = 0
    var isCreated : Bool = false
    var stickerViewOrigin : CGPoint!
    var labelViewOrigin : CGPoint!
    var tap: UITapGestureRecognizer!
    let label = UILabel()
    var labeLTemp = UILabel()
    var colorSuggestions : [UIColor]!
    
    
    let stickerImgArray : [UIImage] = [
        UIImage(named: "sticker")!,
        UIImage(named: "background")!,
        UIImage(named: "sticker")!,
        UIImage(named: "social-media")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!,
        UIImage(named: "sticker")!
    ]
    
    let TextColorArray : [UIColor] = [
       #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1),#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1),#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1),#colorLiteral(red: 1, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 1, green: 0.8979164958, blue: 0, alpha: 1),#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1),#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    ]
    
    let stickers: [UIImage] = [
        #imageLiteral(resourceName: "sticker1"), #imageLiteral(resourceName: "sticker2"), #imageLiteral(resourceName: "sticker3"), #imageLiteral(resourceName: "sticker4"), #imageLiteral(resourceName: "sticker5"), #imageLiteral(resourceName: "sticker6"), #imageLiteral(resourceName: "sticker7"), #imageLiteral(resourceName: "sticker8"), #imageLiteral(resourceName: "sticker9"), #imageLiteral(resourceName: "stcker-flower"), #imageLiteral(resourceName: "sticker11"), #imageLiteral(resourceName: "sticker12"), #imageLiteral(resourceName: "sticker13"), #imageLiteral(resourceName: "sticker14"), #imageLiteral(resourceName: "sticker15")
    ]
    
    let defaults = UserDefaults.standard
    let myTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
    
    var effect : UIVisualEffect!
    var BGCcollectionViewFlowLayout : UICollectionViewFlowLayout!
    var TCcollectionViewFlowLayout : UICollectionViewFlowLayout!
    var StickerCollectionViewFlowLayout : UICollectionViewFlowLayout!
    
    var templateSender: IndexPath?
    var templateRow: Int?
    var templateToDisplay: Template!
    
    var viewGenerator: ViewGenerator?
    var tempImage: UIImageView?
   
    @IBOutlet weak var btnCloseChoosePhoto: UIButton!
    @IBOutlet weak var btnChooseGalleryOutlet: UIButton!
    //==================================================//
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
// initialize label container n place
        label.layer.borderWidth = 0
        label.layer.borderColor = UIColor.black.cgColor
        setDefaultText()
        setTextGesture()
        
        colorSuggestions = templateToDisplay.bgColorSuggestions

// corner radius button text color
        textColorBtn.layer.cornerRadius = 5
// hapus semua view selain design tool bar
        hidallview()
        
// CollectionView Sticker
        setUpStickerCV()
// CollectionView TextColor
        setUpTextColorCV()
// CollectView Backgroundcolor
        setUpBackgroundColorCV()

//munculin view design toolbar
        addDesignToolbar()
//ilangin tab bar
        
//save button effect
        effect = visualEffectView.effect
        visualEffectView.effect = nil
        viewSaveOutlet.layer.cornerRadius = 5
//Set logo di nav bar
        setUpMenuButton()
//DismissKeyboard
        
//NIXI'S
        setUpDirectory()
        
        //set Main View
        viewGenerator = ViewGenerator(mainView: viewCanvas, mainUIViewController: self, templateName: templateToDisplay)
        self.setupToHideKeyboardOnTapOnView()
    }
//====================================================//
    
    @IBAction func actionBtnSaveDesign_Click(_ sender: Any) {
       
        let image = viewCanvas.asImage()
        
        // Save design to Apps Documents/SavedDesigns folder
        var imagePath = NSDate().description
        imagePath = imagePath.replacingOccurrences(of: " ", with: "")
        imagePath = imagesDirectoryPath+("/\(imagePath).png")
        let data = image.pngData()
        let success = FileManager.default.createFile(atPath: imagePath, contents: data, attributes: nil)
        
        do {
            titles = try FileManager.default.contentsOfDirectory(atPath: imagesDirectoryPath)
            defaults.set(titles, forKey: "directoryImage")
            for image in titles{
                let imagePath = imagesDirectoryPath+("/\(image)")
            }
        } catch {
            print("Error")
        }
        
        // Save design to User Photo Gallery
        //guard let image = UIImage(named:"Group.png") else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @IBAction func textAlignmentTapped(_ sender: Any) {
        if isCreated == true{
            if label.textAlignment == .center{
                label.textAlignment = .left
                textAlignmentOutlet.setBackgroundImage(UIImage(systemName: "text.alignleft"), for: .normal)
            }
            else if label.textAlignment == .left{
                label.textAlignment = .right
                textAlignmentOutlet.setBackgroundImage(UIImage(systemName: "text.alignright"), for: .normal)
            }
            else if label.textAlignment == .right{
                label.textAlignment = .center
                textAlignmentOutlet.setBackgroundImage(UIImage(systemName: "text.aligncenter"), for: .normal)
                
            }
        }
    }
    
    @IBAction func deleteTextTapped(_ sender: Any) {
        if (isCreated == true){
            setDefaultText()
            resetTextfield()
            label.removeFromSuperview()
            removeTextToolbar()
            addDesignToolbar()
            isCreated = false
        }
    }
    
    @IBAction func actionBtnSaveInstagram_Click(_ sender: Any) {
    
        viewGenerator!.shareToInstagramStories()

    }
// MUNCULIN TAB BAR
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func closeViewStickerToolbar(_ sender: Any) {
        viewStickerToolbarOutlet.isHidden = true
        addDesignToolbar()
    }
    
    @IBAction func stickerButtonTapped(_ sender: Any) {
        openStickerToolbar()
    }
    
    @IBAction func colorTextToolbarTapped(_ sender: Any) {
        if viewColorTextToolbarOutlet.isHidden == true{
            openTextColor()
            
        }else if viewColorTextToolbarOutlet.isHidden == false{
            closeColorTextToolbar()
        }
    }
    
    @IBAction func closeSaveTapped(_ sender: Any) {
        animateOut()
    }
    func setDismissKeyboard(){
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @IBAction func textToolbarTapped(_ sender: Any) {
        setDismissKeyboard()
        openTextToolbar()
        
        if isCreated == false{
            showTextField()
        }
    }
    
    @IBAction func BackgroundColorTapped(_ sender: Any) {
        viewBackgroundColorToolbar.isHidden = false
        self.view.addSubview(viewBackgroundColorToolbar)
        viewBackgroundColorToolbar.center = CGPoint(x:207.0 , y: 817.0)
        removeDesignToolbar()
    }
    func resetTextfield(){
        myTextField.text = nil
        myTextField.placeholder = "Input Text here"
    }
    
    @objc func saveButtonTapped(){
        self.view.addSubview(visualEffectView)
        visualEffectView.center = self.view.center
        animateIn()
        //animateInChoosePhoto()
    }
    
    func showTextField(){
        myTextField.placeholder = "Input Text here"
        myTextField.textAlignment = .center
        myTextField.layer.borderWidth = 2
        myTextField.layer.borderColor = UIColor.black.cgColor
        viewCanvas.addSubview(myTextField)
        myTextField.center = CGPoint(x: 170, y: 300)
    }
    
    func addTextTo() {
        viewCanvas.addSubview(label)
    }

    @objc func labelTapped(_ sender: UITapGestureRecognizer) {
        openTextToolbar()
        setDismissKeyboard()
        showTextField()
    }
    
    @IBAction func actionBtnChooseGallery_Click(_ sender: Any) {
        print(sender)
        animateOutChoosePhoto()
        viewGenerator?.callChoosePhoto(param: tempImage!)
        
    }
    
    @IBAction func actionBtnCloseChoosePhoto_Click(_ sender: Any) {
        animateOutChoosePhoto()
    }
    func animateOutChoosePhoto(){
        UIView.animate(withDuration: 0.3, animations:{
            self.viewChoosePhoto.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.visualEffectView.effect = nil
        }) {(success: Bool) in self.viewChoosePhoto.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
            self.navigationController?.navigationBar.alpha = 1
        }
    }
    
    func animateInChoosePhoto(paramImage: UIImageView){
        
        //put object image to temporary object
        tempImage = paramImage
        
        self.view.addSubview(viewChoosePhoto)
        viewChoosePhoto.center = self.view.center
        viewChoosePhoto.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewChoosePhoto.alpha = 0
        self.navigationController?.navigationBar.alpha = 1
        
        UIView.animate(withDuration: 0.6){
            self.visualEffectView.effect = self.effect
            self.viewChoosePhoto.alpha = 1
            self.viewChoosePhoto.transform = CGAffineTransform.identity
            self.navigationController?.navigationBar.alpha = 0
        }
        
    }
   
    func setDefaultText(){
        label.frame = CGRect(x: 25, y: 200, width: 300.0, height: 30.0)
        label.textAlignment = .center
        label.transform = CGAffineTransform(rotationAngle: 0)
        textAlignmentOutlet.setBackgroundImage(UIImage(systemName: "text.aligncenter"), for: .normal)
        label.textColor = UIColor.black
        label.font = templateToDisplay.arrayOfText[0].font
        
    }
    func setTextGesture(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
               labelTap.numberOfTapsRequired = 1
               label.addGestureRecognizer(labelTap)

               
               let labelPan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)))
               label.addGestureRecognizer(labelPan)
               labelViewOrigin = label.frame.origin
               
               let labelRotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate))
               label.addGestureRecognizer(labelRotate)
               
               let labelPinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
               label.addGestureRecognizer(labelPinch)
               
               label.isUserInteractionEnabled = true
               label.isMultipleTouchEnabled = true
    }
    
    func animateIn(){
        self.view.addSubview(viewSaveOutlet)
        viewSaveOutlet.center = self.view.center
        viewSaveOutlet.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        viewSaveOutlet.alpha = 0
        self.navigationController?.navigationBar.alpha = 1
        
        UIView.animate(withDuration: 0.6){
            self.visualEffectView.effect = self.effect
            self.viewSaveOutlet.alpha = 1
            self.viewSaveOutlet.transform = CGAffineTransform.identity
            self.navigationController?.navigationBar.alpha = 0
        }
        
    }
    
    func animateOut(){
        UIView.animate(withDuration: 0.3, animations:{
            self.viewSaveOutlet.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.visualEffectView.effect = nil
        }) {(success: Bool) in self.viewSaveOutlet.removeFromSuperview()
            self.visualEffectView.removeFromSuperview()
            self.navigationController?.navigationBar.alpha = 1
        }
    }
    
    func openTextToolbar(){
        isOpen = 1
        removeDesignToolbar()
        self.view.addSubview(viewTextToolOulet)
        viewColorTextToolbarOutlet.isHidden = true
        viewTextToolOulet.isHidden = false
        viewBackgroundColorToolbar.isHidden = true
        viewTextToolOulet.center = CGPoint(x:207.0 , y: 837.0)
    }
    
    func openTextColor(){
        removeDesignToolbar()
        self.view.addSubview(viewColorTextToolbarOutlet)
        viewColorTextToolbarOutlet.isHidden = false
        viewColorTextToolbarOutlet.center = CGPoint(x:207.0 , y: 787.0)
    }
    func closeColorTextToolbar(){
        viewColorTextToolbarOutlet.isHidden = true
        removeDesignToolbar()
    }
    
    func openStickerToolbar(){
        self.view.addSubview(viewStickerToolbarOutlet)
        viewStickerToolbarOutlet.isHidden = false
        viewStickerToolbarOutlet.center = CGPoint(x:207.0 , y: 537.0)
        removeDesignToolbar()
    }
    
    func removeDesignToolbar(){
        viewDesignToolbarOutlet.isHidden = true
        self.viewDesignToolbarOutlet.removeFromSuperview()
    }
    
    func addDesignToolbar(){
        self.view.addSubview(viewDesignToolbarOutlet)
        viewDesignToolbarOutlet.isHidden = false
        viewDesignToolbarOutlet.center = CGPoint(x:207.0 ,y: 814.0)
    }
    func removeTextToolbar(){
        viewTextToolOulet.isHidden = true
        viewColorTextToolbarOutlet.isHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
           //location is relative to the current view
           // do something with the touched point
        if viewTextToolOulet.isHidden == false && isOpen == 0 && (touch?.view != viewTextToolOulet && touch?.view != viewColorTextToolbarOutlet)  {
            removeTextToolbar()
            addDesignToolbar()
            print("abc")
        }
        else if viewBackgroundColorToolbar.isHidden == false && touch?.view != viewBackgroundColorToolbar {
            viewBackgroundColorToolbar.isHidden = true
            addDesignToolbar()
        }

    }
    
    
    
    func setUpMenuButton(){
        let saveBtn = UIButton(type: .custom)
        //saveBtn.tintColor = UIColor.init(named: "black")
        saveBtn.frame = CGRect(x: 0.0, y: 0.0, width: 25, height: 25)
        saveBtn.setImage(UIImage(named: "save"),for: .normal)
        saveBtn.addTarget(self, action: #selector(saveButtonTapped), for: UIControl.Event.touchUpInside)

        let saveBarItem = UIBarButtonItem(customView: saveBtn)
        let currWidth = saveBarItem.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = saveBarItem.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        navigationItem.rightBarButtonItem = saveBarItem
    }
    
    func setUpBackgroundColorCV(){
        BackgroundColorCollectionView.delegate = self
             BackgroundColorCollectionView.dataSource = self
             let nib = UINib(nibName: "BackgroundColorCollectionViewCellController", bundle: nil)
             BackgroundColorCollectionView.register(nib, forCellWithReuseIdentifier: "ColorCell")
        
        
        if BGCcollectionViewFlowLayout == nil{
            let numberOfItemPerRow: CGFloat = 10
            let lineSpacing: CGFloat = 5
            let interItemSpacing : CGFloat = 5
            
            let width = (BackgroundColorCollectionView.frame.width - (numberOfItemPerRow - 1) * interItemSpacing) /
                        numberOfItemPerRow
            let height = width
            
            BGCcollectionViewFlowLayout = UICollectionViewFlowLayout()
            BGCcollectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            BGCcollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
            BGCcollectionViewFlowLayout.scrollDirection = .vertical
            BGCcollectionViewFlowLayout.minimumLineSpacing = lineSpacing
            BGCcollectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            BackgroundColorCollectionView.setCollectionViewLayout(BGCcollectionViewFlowLayout, animated: true)
        }
    }
    
    func setUpTextColorCV(){
        TextColorCollectionView.dataSource = self
        TextColorCollectionView.delegate = self
        let nib = UINib(nibName: "TextColorCollectionViewCellController", bundle: nil)
        TextColorCollectionView.register(nib, forCellWithReuseIdentifier: "TextColorCell")
        
        if TCcollectionViewFlowLayout == nil{
            let numberOfItemPerRow: CGFloat = 10
            let lineSpacing: CGFloat = 5
            let interItemSpacing : CGFloat = 10
            
            let width = (TextColorCollectionView.frame.width - (numberOfItemPerRow - 1) * interItemSpacing) /
                        numberOfItemPerRow
            let height = width
            
            TCcollectionViewFlowLayout = UICollectionViewFlowLayout()
            TCcollectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            TCcollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
            TCcollectionViewFlowLayout.scrollDirection = .vertical
            TCcollectionViewFlowLayout.minimumLineSpacing = lineSpacing
            TCcollectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            TextColorCollectionView.setCollectionViewLayout(TCcollectionViewFlowLayout, animated: true)
        }
        
        
    }
    
    func setUpStickerCV(){
        StickerCollectionView.delegate = self
        StickerCollectionView.dataSource = self
        
    let nib = UINib(nibName: "StickerCollectionViewCellController", bundle: nil)
        StickerCollectionView.register(nib, forCellWithReuseIdentifier: "StickerCell")
        
        if StickerCollectionViewFlowLayout == nil{
            let numberOfItemPerRow: CGFloat = 3
            let lineSpacing: CGFloat = 10
            let interItemSpacing : CGFloat = 10
            
            let width = (TextColorCollectionView.frame.width - (numberOfItemPerRow - 1) * interItemSpacing) /
                        numberOfItemPerRow
            let height = width
            
            StickerCollectionViewFlowLayout = UICollectionViewFlowLayout()
            StickerCollectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            StickerCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 5, left: 20, bottom: 0, right: 20)
            StickerCollectionViewFlowLayout.scrollDirection = .vertical
            StickerCollectionViewFlowLayout.minimumLineSpacing = lineSpacing
            StickerCollectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            StickerCollectionView.setCollectionViewLayout(StickerCollectionViewFlowLayout, animated: true)
        }
        
    }

    
    func hidallview(){
           viewTextToolOulet.isHidden = true
           viewStickerToolbarOutlet.isHidden = true
           viewBackgroundColorToolbar.isHidden = true
       }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        isOpen = 0
        myTextField.removeFromSuperview()
        if myTextField.text != ""{
            label.text = myTextField.text
            addTextTo()
            isCreated = true
        }else if myTextField.text == ""{
            viewTextToolOulet.isHidden = true
            addDesignToolbar()
        }
        view.removeGestureRecognizer(tap)
    }
    
//Collection View Function
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == TextColorCollectionView){
                   return TextColorArray.count
               }
        else if (collectionView == BackgroundColorCollectionView){
            return colorSuggestions.count
        }
        return stickers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == TextColorCollectionView){
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "TextColorCell", for: indexPath) as! TextColorCollectionViewCellController
            cell3.TextColorImage.image = UIImage(systemName: "circle.fill")
            cell3.TextColorImage.tintColor = TextColorArray[indexPath.item]
            
            print("ini TExt Color bang")
            return cell3
        }
        else if (collectionView == BackgroundColorCollectionView){
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as! BackgroundColorCollectionViewCellController
            cell2.backgroundColorPalletImg.image = UIImage(systemName: "circle.fill")
            cell2.backgroundColorPalletImg.tintColor = colorSuggestions[indexPath.item]
            
            
            return cell2
        }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! StickerCollectionViewCellController
            
            cell.StickerImage.image = stickers[indexPath.item]
            
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView == StickerCollectionView){
            let stickerImageView = UIImageView()
            stickerImageView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            stickerImageView.image = stickers[indexPath.item]
            
            let stickerTap = UITapGestureRecognizer(target: self, action: #selector(self.stickerTapped(_:)))
            stickerImageView.addGestureRecognizer(stickerTap)
            
            addPanGesture(view: stickerImageView)
            stickerViewOrigin = stickerImageView.frame.origin
            
            let stickerRotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate))
            stickerImageView.addGestureRecognizer(stickerRotate)
            
            let stickerPinch = UIPinchGestureRecognizer(target: self, action: #selector(handlePinch))
            stickerImageView.addGestureRecognizer(stickerPinch)
            
            stickerImageView.isUserInteractionEnabled = true
            stickerImageView.isMultipleTouchEnabled = true
            
            viewCanvas.addSubview(stickerImageView)
            
            viewStickerToolbarOutlet.isHidden = true
            addDesignToolbar()
        }
        else if(collectionView == TextColorCollectionView){
            
            label.textColor = TextColorArray[indexPath.item]

        }
        else if(collectionView == BackgroundColorCollectionView){
            viewCanvas.backgroundColor = colorSuggestions[indexPath.item]
        }
    }
    
    
    
    @objc func handlePinch(recognizer : UIPinchGestureRecognizer){
        if let view = recognizer.view{
            view.transform = view.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            recognizer.scale = 1
        }
    }
    
    @objc func handleRotate(recognizer : UIRotationGestureRecognizer){
        if let view = recognizer.view{
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    func addPanGesture(view : UIView){
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(sender:)))
        view.addGestureRecognizer(pan)
    }
    
    @objc func handlePan (sender : UIPanGestureRecognizer){
        let stickerView = sender.view!
        let translation = sender.translation(in: view)
        
        switch sender.state {
        case .began, .changed:
            stickerView.center = CGPoint(x: stickerView.center.x + translation.x , y: stickerView.center.y + translation.y)
            sender.setTranslation(CGPoint.zero, in: view)
        case .ended :
            break
        default:
            break
        }
        
    }
    
    @objc func stickerTapped(_ sender: UITapGestureRecognizer) {
           print("stickerTapped")
    
    }
   
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        animateOut()
         if let error = error {
             let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
             ac.addAction(UIAlertAction(title:"OK",style:.default))
         } else {
             let ac = UIAlertController(title: "Saved!", message: "The design has been saved to your photos.", preferredStyle: .alert)
             ac.addAction(UIAlertAction(title:"OK",style:.default))
             present(ac, animated: true)
         }
     }
    
     func setUpDirectory() {
            images = []
             
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            // Get the Document directory path
            let documentDirectoryPath:String = paths[0]
            // Create a new path for the new images folder
            imagesDirectoryPath = documentDirectoryPath+("/SavedDesigns")
            var objcBool:ObjCBool = true
            let isExist = FileManager.default.fileExists(atPath: imagesDirectoryPath, isDirectory: &objcBool)
            // If the folder with the given path doesn't exist already, create it
            if isExist == false{
              do{
                try FileManager.default.createDirectory(atPath: imagesDirectoryPath, withIntermediateDirectories: true, attributes: nil)
                }catch{
                  print("Something went wrong while creating a new folder")
                }
            }
        }
    
    
}

extension UIColor{
    public class var main:  UIColor{
        return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
}

