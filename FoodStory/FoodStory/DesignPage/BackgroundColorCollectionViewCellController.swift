//
//  BackgroundColorCollectionViewCellController.swift
//  FoodStory
//
//  Created by Adhitya Yoga Yudanto on 22/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class BackgroundColorCollectionViewCellController: UICollectionViewCell {

    @IBOutlet weak var backgroundColorPalletImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
