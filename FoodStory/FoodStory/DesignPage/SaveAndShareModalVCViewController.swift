//
//  SaveAndShareModalVCViewController.swift
//  FoodStory
//
//  Created by Adhitya Yoga Yudanto on 21/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit
import Photos

class SaveAndShareModalVCViewController: UIViewController {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSaveInstagram: UIButton!
    @IBOutlet var viewMain: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func funcBtnClose_TouchUpInside(_ sender: UIButton) {
        dismiss(animated: true)
        
    }
    
    @IBAction func actionBtnSaveInstagram_Click(_ sender: Any) {
    
        let exImage = UIImage(named: "Group.png")
        
        shareToInstagramStories(image: exImage!)
        
    }
    
    //Note : if show an error about permission, you have to add LSApplicationQueriesSchemes into info.plist
    //Ref : https://developers.facebook.com/docs/instagram/sharing-to-stories
    func shareToInstagramStories(image: UIImage) {
        // NOTE: you need a different custom URL scheme for Stories, instagram-stories, add it to your Info.plist!
        guard let instagramUrl = URL(string: "instagram-stories://share") else {
            return
        }

        if UIApplication.shared.canOpenURL(instagramUrl) {
            let pasterboardItems = [["com.instagram.sharedSticker.backgroundImage": image as Any]]
            UIPasteboard.general.setItems(pasterboardItems)
            UIApplication.shared.open(instagramUrl)
        } else {
            // Instagram app is not installed or can't be opened, pop up an alert
        }
    }
    
}
