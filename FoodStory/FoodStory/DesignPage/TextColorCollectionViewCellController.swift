//
//  TextColorCollectionViewCellController.swift
//  FoodStory
//
//  Created by Glendito Jeremiah Palendeng on 22/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class TextColorCollectionViewCellController: UICollectionViewCell {

    @IBOutlet weak var TextColorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
