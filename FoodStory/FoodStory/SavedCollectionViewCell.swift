//
//  SavedCollectionViewCell.swift
//  FoodStory
//
//  Created by Davia Belinda Hidayat on 20/05/20.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class SavedCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var savedImageName: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    

}
