//
//  DiscoverTableViewCell.swift
//  FoodStory
//
//  Created by Abby on 2020-05-22.
//  Copyright © 2020 Nixi Sendya Putri. All rights reserved.
//

import UIKit

class DiscoverTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelThemeTitle: UILabel!
    
    var models = [UIImage]()
    var template = [Template]()
    var didSelectItemAction: ((Template) -> Void)?
    var templateToDisplay: Template!
    
    func configure(with models: [UIImage], template: [Template]) {
        self.models = models
        self.template = template
        collectionView.reloadData()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.register(DiscoverCollectionViewCell.nib(), forCellWithReuseIdentifier: DiscoverCollectionViewCell.identifier)
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}

extension DiscoverTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:
            DiscoverCollectionViewCell.identifier, for: indexPath) as! DiscoverCollectionViewCell
        cell.configure(with: models[indexPath.row])
        return cell
}
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        templateToDisplay = template[indexPath.row]
        didSelectItemAction?(templateToDisplay)
    }
}
